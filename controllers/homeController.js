// Display root page
exports.index = (req, res, next) => {
	res.render('home/index', {layout: 'layouts/main', cat: "7", dog: "bowwow"});
};

exports.checkout = (req, res, next) => {
	res.render('home/checkout', {layout: 'layouts/main'});
};
