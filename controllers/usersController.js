// JSON routes to interact with adding and removing people to answer the phone.
const CallRecipient=require('../models/CallRecipient');
const CalendarSlots = require('../models/CalendarSlots');
//Returns all user in the account.
exports.all = (req, res, next) => {
	//Find all users for a certain account id.
	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
		const account_id = body.account_id;
		console.log(body);

		CallRecipient.find({account_id: account_id}, function(error, event){
			console.log(event);
			res.json(event);
		})
	});
	//res.json({all_users_for_this_account: "All the users"});
};


//Returns full calendar in response
exports.add = (req, res, next) => {
	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
		console.log(body);

		CallRecipient.create(body, function(error, event){
			if(error){
				return next(error);
			}
			else{
				console.log('Created event.  event is:');
				console.log(event);
				res.json(event);
			}
		})
	});
//Return all the users that are in the account.
//const account_id = req.user.account_id;

};

exports.edit = (req, res, next) => {
	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
		console.log(body);
		let setObj={
			name: body.name,
			number: body.number,
			color: body.color,
		}
		CallRecipient.update({account_id: body.account_id, _id: body._id },{ $set: setObj }, function(error, event){
			if(error){
				return next(error);
			}
			else{
				console.log('edit event.  event is:');
				console.log(event);
				res.json(event);
			}
		});
	});
//Return all the users that are in the account.
};

exports.remove = (req, res, next) => {
	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
		console.log(body);
		//name, _id, account_id, number, color (all strings)
		console.log('the remove event, dog!')
		CallRecipient.deleteMany({account_id: body.account_id, _id: body._id }, function (err) { //delete any calendar records
		  	if (err) return handleError(err);
		  	console.log('removed ' + body.name + ' from list of "recipients".')
			CalendarSlots.deleteMany({account_id: body.account_id, recipient_id: body._id }, function (err) { //delete any calendar records
				if (err) return handleError(err);
				console.log('removed any calendar entries that ' + body.name + ' had.')
				res.json({success: true});
			});
		});	
	});
};