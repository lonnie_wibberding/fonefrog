var moment = require('moment-timezone');
var Account = require('../models/Account');
var bcrypt = require('bcrypt');


// exports.register = (req, res, next) => {
// 	timezones = moment.tz.names();
// 	// Add United States timezones at the top.
// 	timezones.unshift('US/Alaska', 'US/Pacific', 'US/Central', 'US/Eastern');
// 	res.render('sessions/register', {timezones: timezones, layout: 'layouts/main'});
// };
//
// exports.login = (req, res, next) => {
// 	res.render('sessions/login', {tester: ' home', layout: 'layouts/main'});
// };

exports.register = (req, res, next) => {
	res.render('session/register',{layout: 'layouts/main'});
};

exports.login = (req, res, next) => {
	res.render('session/login',{layout: 'layouts/main'});
};

exports.forgot_password = (req, res, next) => {
	res.render('session/forgot_password',{layout: 'layouts/main'});
};

exports.process_registration1 = (req, res, next)=>{
	const body=req.body;
	console.log(body);

	//Hash and salt the password.
	const password = body.password;
	const saltRounds = 12;
	bcrypt.hash(password, saltRounds, function(err, hash) {
		let obj ={
			time_zone: body.timeZone,
			customer_first_name: body.firstn,
			customer_last_name: body.lastn,
			email: body.email,
			password_hash: hash
		};
		Account.create(obj, (error, data)=>{
			console.log('account created');
			data.message = "Account created successfully";
			res.json(data);
		})
	});
};

exports.process_registration2 = (req, res, next)=>{
	const body=req.body;
	console.log(body);
	console.log('we got a client.  We will be rich now! :D ');
	res.json(body);
};
