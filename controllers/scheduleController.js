var CalendarSlots = require('../models/CalendarSlots');
// JSON routes to interact with the forwarding schedule for each account.

exports.index = (req, res, next) => {
	console.log('release the derpin!')
	//respond with full calendar in json format

	//const account_id = req.account_id;
	//const json_to_return = CalendarSlots.get_calendar(account_id);
	//res.json(json_to_return);
};

//Returns full calendar in response
exports.stored_schedule = (req, res, next) => {
	console.log('release the derpin!')
	//respond with full calendar in json format
	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); 
		const account_id=body.account_id;
		CalendarSlots.find({account_id: body.account_id}, function(error, event){
			console.log(event);
			res.json(event);
		});
	});
	//const account_id = req.account_id;
	//const json_to_return = CalendarSlots.get_calendar(account_id);
	//res.json(json_to_return);
};

exports.create_slot = (req, res, next) => {
	//request should contain name id of the callRecipient, day, and hour to be created.
	//respond with full calendar in json format
	console.log(req.body);

	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
		//console.log('check out the body now!')
		//console.log(body);

		CalendarSlots.find({account_id: body.account_id, recipient_id: body.recipient_id }, function(error, event){
			let times=[];
			//let accountFound = false;
			for(let e=0; e < event.length; e++){
				if(event[e].recipient_id===body.recipient_id){
					//console.log(event[e].times);
					//accountFound=true;
					times=event[e].times;
					break;
				}
			}
			//if we don't have an "account" for this recipient id, it means there is no entry.
			//if we do have an entry, we must have already made an account for this recipient_id.
			//if they have no entries, in that same situation we should remove the "account" in the remove event ^_^
			if(event.length===0){ 
				let obj={
					account_id: body.account_id,
					recipient_id: body.recipient_id,
					name: body.name,
					times: [body.pointObj]
				}
				CalendarSlots.create(obj, (error, data)=>{
					console.log('46.  return creation data: ');
					console.log(data);
				});
			}
			else{
				const newTimes=getWithTheTimes(event, body, "add");
				console.log(newTimes);
				console.log('newTimes length is: ' + newTimes.length);
				CalendarSlots.update({account_id: body.account_id, recipient_id: body.recipient_id },
				{ $set: {times: newTimes} }, (error, data)=>{
					//console.log('54.  return creation data: ');
					//console.log(data);
					res.json(body);
				});
			}
		});
		
	});
	//const account_id = req.call_recipeint.account_id;
	//const json_to_return = CalendarSlots.get_calendar(account_id);
	//res.json(json_to_return);
	
};

exports.remove_slot = (req, res, next) => {
	let arrivingData='';
	req.on('data', (data)=>{
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
		console.log(body);

		CalendarSlots.find({account_id: body.account_id, recipient_id: body.recipient_id }, function(error, event){
			//console.log('line 83, removeslot.  event return is: ');
			//console.log(event);

			let times=[];
			//let accountFound = false;
			for(let e=0; e < event.length; e++){
				if(event[e].recipient_id===body.recipient_id){
					//console.log('the times WAS: ')
					//console.log(event[e].times);
					//accountFound=true;
					times=event[e].times;
					break;
				}
			}
			//if we don't have an "account" for this recipient id, it means there is no entry.
			//if we do have an entry, we must have already made an account for this recipient_id.
			//if they have no entries, in that same situation we should remove the "account" in the remove event ^_^
			if(event.length>0){ 
				const newTimes=getWithTheTimes(event, body, "remove");
				console.log(newTimes);
				console.log('newTimes length is: ' + newTimes.length);
				if(newTimes.length===0){ //if we won't have an more times, remove the "account"
					CalendarSlots.remove({account_id: body.account_id, recipient_id: body.recipient_id },(error, event)=>{
						//if error throw error;
						console.log('method to remove "account" for ' + body.account_id + ", " + body.recipient_id + " is:");
						console.log(event);
						console.log("'account' removed")
						res.json({status: "'account' removed"})
					})
				}
				else{ //otherwise, update the new values ^_^
					CalendarSlots.update({account_id: body.account_id, recipient_id: body.recipient_id },
					{ $set: {times: newTimes} }, (error, data)=>{
						console.log('newTimes is: ');
						console.log(newTimes);
						console.log('but the old times is: ');
						console.log(times);
						//console.log('54.  return creation data: ');
						//console.log(data);
						console.log("successful removal")
						res.json({status: "successful removal"});
					});
				}
			}
			else{
				console.log("no 'accounts' to delete!")
				res.json({status: "no 'accounts' to delete!"});
			}
		});
		
	});
	
};

function getWithTheTimes(event, body, action="times"){
	let times=[];
	for(let e=0; e < event.length; e++){
		if(event[e].recipient_id===body.recipient_id){
			times=event[e].times;
			break;
		}
	}

	switch(action){
		case "times":
			return(times);
		break;

		case "add":
			times.push(body.pointObj);
			return(times);
		break;

		case "remove":
			for(let i=0; i < times.length; i++){
				if((times[i].day===body.pointObj.day)&&(times[i].hour===body.pointObj.hour)){
					times.splice(i,1);
					return(times);
				}
			}
			return(times);
		break;
	}
}