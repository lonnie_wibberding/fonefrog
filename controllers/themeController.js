// Display root page
exports.index = (req, res, next) => {
	res.render('theme_template_pages/index', {layout: 'layouts/main'});
};
exports.about = (req, res, next) => {
	res.render('theme_template_pages/about', {layout: 'layouts/main'});
};
exports.blog_details = (req, res, next) => {
	res.render('theme_template_pages/blog-details', {layout: 'layouts/main'});
};
exports.blog = (req, res, next) => {
	res.render('theme_template_pages/blog', {layout: 'layouts/main'});
};
exports.contact = (req, res, next) => {
	res.render('theme_template_pages/contact', {layout: 'layouts/main'});
};
exports.faq = (req, res, next) => {
	res.render('theme_template_pages/', {layout: 'layouts/main'});
};
exports.index_2 = (req, res, next) => {
	res.render('theme_template_pages/index-2', {layout: 'layouts/main'});
};
exports.pricing = (req, res, next) => {
	res.render('theme_template_pages/pricing', {layout: 'layouts/main'});
};
exports.projects_detail = (req, res, next) => {
	res.render('theme_template_pages/projects-detail', {layout: 'layouts/main'});
};
exports.projects = (req, res, next) => {
	res.render('theme_template_pages/projects', {layout: 'layouts/main'});
};
exports.services_detail = (req, res, next) => {
	res.render('theme_template_pages/services-detail', {layout: 'layouts/main'});
};
exports.services = (req, res, next) => {
	res.render('theme_template_pages/services', {layout: 'layouts/main'});
};
exports.shop_details = (req, res, next) => {
	res.render('theme_template_pages/shop-details', {layout: 'layouts/main'});
};
exports.shop = (req, res, next) => {
	res.render('theme_template_pages/shop', {layout: 'layouts/main'});
};
exports.team_details = (req, res, next) => {
	res.render('theme_template_pages/team-details', {layout: 'layouts/main'});
};
exports.team = (req, res, next) => {
	res.render('theme_template_pages/team', {layout: 'layouts/main'});
};
exports.thank_you = (req, res, next) => {
	res.render('theme_template_pages/thank-you', {layout: 'layouts/main'});
};
