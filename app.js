const express = require('express');
const app = express(); //for post get (etc) requests ^_^
const PORT = process.env.PORT || 3000;
const path= require('path');
var bodyParser = require('body-parser');
require('dotenv').config();
const session = require('express-session');  // session middleware
const passport = require('passport');  // authentication
const connectEnsureLogin = require('connect-ensure-login'); //authorization
let User = require('./models/User');


//Dan's mongoose connect ^_^
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI,{ //process.env.MONGODB_URI
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.on('connected',()=>{
	console.log('connected with a mongoose')
});

//Require database
var db = require('./database') //imports mongoose with database connection

//Dan's stripe section on the server ^_^
app.use('/payment', express.json());
//app.use(express.json());

const stripe = require('stripe')(process.env.STRIPE_PRIVATE_KEY);
const storeItems = new Map([
	[1, {priceInCents: 10000, name: "Learn React Today"}],
	[2, {priceInCents: 20000, name: "Learn CSS Today"}]
]);

app.post('/payment/create-checkout-session', async (req, res,next)=>{
	try{
		const session = await stripe.checkout.sessions.create({
			payment_method_types: ['card'],
			mode: 'payment', //we'll want subscription!
			line_items: req.body.items.map(item =>{
				const storeItem = storeItems.get(item.id);
				return {
					price_data: {
						currency: 'usd',
						product_data: {
							name: storeItem.name
						},
						unit_amount: storeItem.priceInCents
					},
					quantity: item.quantity
				}
			}), //array of items we want to purchase ^_^
			success_url: `${process.env.SERVER_URL}/success.html`,
			cancel_url: `${process.env.SERVER_URL}/cancel.html`
		});
		res.json({ url: session.url })
	}
	catch(e){
		res.status(500).json({ error: e.message })
	}
});

//lolz, apparently I can have the server file in the same folder as the project.  Yippie! :D
//app.use(express.static('../SassyProject'));  //This was working fine, except I was having trouble getting the form.
app.use('/assets', express.static(path.join(__dirname, '/public')));


app.post('/json/moreexamplepath', (req, res, next)=>{
	let arrivingData='';
	req.on('data', (data)=>{
		console.log('getting data :D');
		arrivingData+=data;
	});

	req.on('end', ()=>{
		const body = JSON.parse(arrivingData); //getting the body from a json requestion
	});
});

//app.use('/form', bodyParser.urlencoded({extended: true}), (req, res, next)=>{ //express.urlencoded({extended: true})
	//console.log('dips and dorps!');
//	next();
//});
const multer  = require('multer')
const upload = multer()
app.use('/form', upload.none(), (req, res, next)=>{ //express.urlencoded({extended: true})
	console.log('clips and clorps!');
	next();
});


// configure the Handlebars view engine
var hbs = require('hbs');
app.set('view engine', 'hbs');


/////////// ROUTING /////////

//Parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Setup sessions for logged in routes
app.use(session({
  secret: 'hard work',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60 * 60 * 1000 } // 1 hour (milliseconds)
}));
app.use(passport.initialize());
app.use(passport.session());



// To use with sessions. 
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Logs requested URL to the terminal
app.use('/', function (req, res, next) {
	console.log('Request Url:' + req.url);
	next();
});

//  Require routes
var homeRoutes = require('./routes/home');
var sessionsRoutes = require('./routes/sessions');
var forwardingRoutes = require('./routes/forwarding');
var billingRoutes = require('./routes/billing');
var themeTestRoutes = require('./routes/theme');
var scheduleRoutes = require('./routes/schedule');
var userRoutes = require('./routes/user');

//  Define routes...
app.use('/', homeRoutes);
app.use('/', sessionsRoutes);
app.use('/forwarding', forwardingRoutes);
app.use('/billing', billingRoutes);
app.use('/theme', themeTestRoutes);
app.use('/schedule', scheduleRoutes);
app.use('/user', userRoutes);

app.listen(PORT, ()=>{
    console.log('big brother is listening');
});
