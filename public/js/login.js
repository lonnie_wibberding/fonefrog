document.getElementById("loginForm").addEventListener("click", (event)=>{
    event.preventDefault();

    var formElement=document.getElementById("loginForm");
    var formData = new FormData(formElement);

    var request = new XMLHttpRequest();
    var url="/form/login"; //for the request :D
    request.open("POST", url);
    request.send(formData);
});