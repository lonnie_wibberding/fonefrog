document.getElementById("submitRegistration1").addEventListener("click", (event)=>{
    event.preventDefault();
    var xhr = new XMLHttpRequest;
    var url="/form/register1"; //for the request :D

    var formElement=document.getElementById("register1");
    var formData = new FormData(formElement);
    formData.append("CustomField", "This is some extra data");
    xhr.responseType='json';
    xhr.onreadystatechange =()=>{
        if(xhr.readyState ===XMLHttpRequest.DONE){
			console.log(xhr.response);
			if (xhr.response.message) {
				alert(xhr.response.message);

			}
			console.log(xhr.response.message)
			addNameToCreditCardInformation(xhr.response);
        }
    }
    xhr.open("POST", url);
    xhr.send(formData);
});

//Adds name information from registration form 1 to registration form 2
function addNameToCreditCardInformation(data) {
	const name = data.customer_first_name + " " + data.customer_last_name;
	document.getElementById("nameOnCard").value = name;
}

document.getElementById("submitRegistration2").addEventListener("click", (event)=>{
    event.preventDefault();
    var xhr = new XMLHttpRequest;
    var url="/form/register2"; //for the request :D

    var formElement=document.getElementById("register2");
    var formData = new FormData(formElement);
    formData.append("CustomField", "This is some extra data");
    xhr.responseType='json';
    xhr.onreadystatechange =()=>{
        if(xhr.readyState ===XMLHttpRequest.DONE){
			console.log(xhr.response);
        }
    }
    xhr.open("POST", url);
    xhr.send(formData);
});
