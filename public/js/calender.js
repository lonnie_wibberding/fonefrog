var Day;
(function (Day) {
    Day[Day["SUNDAY"] = 0] = "SUNDAY";
    Day[Day["MONDAY"] = 1] = "MONDAY";
    Day[Day["TUESDAY"] = 2] = "TUESDAY";
    Day[Day["WEDNESDAY"] = 3] = "WEDNESDAY";
    Day[Day["THURSDAY"] = 4] = "THURSDAY";
    Day[Day["FRIDAY"] = 5] = "FRIDAY";
    Day[Day["SATURDAY"] = 6] = "SATURDAY";
})(Day || (Day = {}));
var account_id = "123456789";
function addEntry(pointObj, name, recipient_id, color, calenderArray) {
    var found = false;
    var recipientFound = false;
    for (var a = 0; a < calenderArray.length; a++) {
        if (calenderArray[a].recipient_id == recipient_id) {
            recipientFound = true;
            //found=false;
            for (var i = 0; i < calenderArray[a].times.length; i++) {
                if ((pointObj.day === calenderArray[a].times[i].day) && (pointObj.hour === calenderArray[a].times[i].hour)) {
                    found = true;
                    break;
                }
            }
            if (found === false) {
                calenderArray[a].times.push(pointObj);
                markCalenderSpot(pointObj, name, recipient_id, color, calenderArray);
                createSlotRequest(pointObj, name, recipient_id);
            }
            else {
                console.log(name + "(" + recipient_id + " )" + 'is already scheduled to work then.');
            }
            break;
        }
    }
    if (recipientFound === false) {
        for (var p = 0; p < fakeCalenderProfile.length; p++) {
            if ((recipient_id !== undefined) && (fakeCalenderProfile[p]._id === recipient_id)) {
                console.log('recipient not found in entries, but found in profiles.  requesting new entry ^_^');
                calenderArray.push({
                    account_id: account_id,
                    recipient_id: fakeCalenderProfile[p]._id,
                    name: fakeCalenderProfile[p].name,
                    times: [pointObj]
                });
                markCalenderSpot(pointObj, name, recipient_id, color, calenderArray);
                createSlotRequest(pointObj, name, recipient_id);
            }
        }
    }
    return (calenderArray);
}
function markCalenderSpot(pointObj, name, recipient_id, color, calenderArray) {
    var eyeDee = "column" + pointObj.day.toString() + "row" + pointObj.hour.toString();
    var el = document.getElementById(eyeDee);
    if (el !== null) {
        var calenderInst = document.createElement("DIV");
        calenderInst.id = eyeDee + "_" + recipient_id;
        calenderInst.className = "calenderMark";
        el.appendChild(calenderInst);
        var calenderDesc = document.createElement("DIV");
        calenderDesc.id = eyeDee + "_" + recipient_id + "~" + "Desc";
        calenderDesc.textContent = name;
        calenderInst.appendChild(calenderDesc);
        var canvasMark_1 = document.createElement("CANVAS");
        canvasMark_1.style.width = "16px";
        canvasMark_1.style.height = "16px";
        canvasMark_1.width = 100;
        canvasMark_1.height = 100;
        canvasMark_1.style.backgroundColor = color;
        //canvasMark.className="calenderMark";
        calenderInst.appendChild(canvasMark_1);
        canvasMark_1.addEventListener("mouseenter", function (event) {
            var ctx = canvasMark_1.getContext("2d");
            if (ctx !== null) {
                ctx.clearRect(0, 0, canvasMark_1.width, canvasMark_1.height);
                ctx.lineWidth = 4;
                ctx.moveTo(10, 10);
                ctx.lineTo(90, 90);
                ctx.stroke();
                ctx.moveTo(10, 90);
                ctx.lineTo(90, 10);
                ctx.stroke();
            }
        });
        canvasMark_1.addEventListener("mouseout", function (event) {
            var ctx = canvasMark_1.getContext("2d");
            if (ctx !== null) {
                ctx.clearRect(0, 0, canvasMark_1.width, canvasMark_1.height);
            }
        });
        canvasMark_1.addEventListener("click", function (event) {
            removeCalenderSpot(pointObj, name, recipient_id, calenderArray);
            console.log(pointObj);
            removeSlotRequest(pointObj, name, recipient_id);
            removedSlot = true;
        });
    }
}
var removedSlot = false; //keep from hitting the add entry click event every time we click on the remove check!
function removeCalenderSpot(pointObj, name, recipient_id, calenderArray) {
    for (var c = 0; c < calenderArray.length; c++) {
        if (calenderArray[c].recipient_id === recipient_id) {
            for (var i = 0; i < calenderArray[c].times.length; i++) {
                if ((pointObj.day === calenderArray[c].times[i].day) && (pointObj.hour === calenderArray[c].times[i].hour)) {
                    var byeByeId = "column" + pointObj.day.toString() + "row" + pointObj.hour.toString() + "_" + recipient_id;
                    var byeByeEl = document.getElementById(byeByeId);
                    if ((byeByeEl !== null) && (byeByeEl.parentElement !== null)) {
                        console.log('before');
                        console.log(calenderArray[c].times);
                        calenderArray[c].times.splice(i, 1); //remove it from the array;
                        console.log('after');
                        console.log(calenderArray[c].times);
                        byeByeEl.parentElement.removeChild(byeByeEl); //remove it from the dom
                    }
                }
            }
            break;
        }
    }
}
var fakeCalenderEntries = [
/*{
    name: "Billy Bob",
    recipient_id: '3sf8fs3k', //should match the _id of the profile array :D
    account_id: account_id,
    times: [
        {day: Day.SUNDAY, hour: 8}, //0-6, 0-23
        {day: Day.SUNDAY, hour: 9},
        {day: Day.SUNDAY, hour: 10},
    ]
},
{
    name: "Sara Pauling",
    recipient_id: '4k56j44j',
    account_id: account_id,
    times: [
        {day: Day.MONDAY, hour: 1}, //0-6, 0-23
        {day: Day.SUNDAY, hour: 15},
        {day: Day.SUNDAY, hour: 22},
    ]
},
{
    name: "Beep Boop",
    recipient_id: '8s9fsaf8a',
    account_id: account_id,
    times: [
        {day: Day.TUESDAY, hour: 11}, //0-6, 0-23
        {day: Day.THURSDAY, hour: 13},
        {day: Day.FRIDAY, hour: 15},
    ]
}*/
];
var profileIndex = 0;
var fakeCalenderProfile = [
/*{
    name: "Billy Bob",
    _id: '3sf8fs3k',
    account_id: account_id,
    number: "867-5309",
    color: "#888888"
},
{
    name: "Sara Pauling",
    _id: '4k56j44j',
    account_id: account_id,
    number: "867-5309",
    color: "#E831CA"
},
{
    name: "Beep Boop",
    _id: '8s9fsaf8a',
    account_id: account_id,
    number: "867-5309",
    color: "#E83140"
}*/
];
var colors = [
    { color: '#E83140', name: 'red' },
    { color: '#31E840', name: 'green' },
    { color: '#317AE8', name: 'blue' },
    { color: '#E831CA', name: 'pink' },
    { color: '#C331E8', name: 'purple' },
    { color: '#5C31E8', name: 'violet' },
    { color: '#31E8E2', name: 'aqua' },
    { color: '#E89031', name: 'orange' },
    { color: '#EAF03E', name: 'yellow' },
    { color: '#000000', name: 'black' },
    { color: '#888888', name: 'gray' },
    { color: '#7D4E12', name: 'brown' },
];
var availableColors = colors; //it starts out the same ^_^
function updateAvailableColors(array) {
    if (Array.isArray(array)) {
        if (array.length > 0) {
            for (var a = 0; a < array.length; a++) {
                for (var v = 0; v < availableColors.length; v++) {
                    if (array[a].color === availableColors[v].color) {
                        availableColors.splice(v, 1);
                    }
                }
            }
            console.log('availableColors is:');
            console.log(availableColors);
        }
    }
}
//addUser_request, editUserRequest, removeUser_request
function createColorDropdown() {
    updateAvailableColors(fakeCalenderProfile); //update availableColors array ^_^
    var profileColor = document.getElementById("profileColor");
    var inst;
    //alert(availableColors.length); //9, correct
    removeAllChildNodes(profileColor);
    for (var v = 0; v < availableColors.length; v++) { //fill in the color select element
        inst = document.createElement("option");
        inst.value = availableColors[v].color;
        inst.textContent = availableColors[v].name;
        profileColor.appendChild(inst);
    }
}
function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
/*function thistaster() {
  setTimeout(()=>{
    updateAvailableColors(fakeCalenderProfile)
  }, 3000);
}
thistaster();*/
function userSelecterSetup(selectEl) {
    var inst;
    for (var f = 0; f < fakeCalenderProfile.length; f++) { //fill in the select user select element, and set the value to indexes ^_^
        inst = document.createElement("option");
        inst.value = f.toString();
        inst.textContent = fakeCalenderProfile[f].name + ", " + fakeCalenderProfile[f].number;
        selectEl.appendChild(inst);
    }
}
function setupCalenderButtonControl() {
    var userSelecter = document.getElementById("userSelecter");
    var profileName = document.getElementById("profileName");
    var profileNumber = document.getElementById("profileNumber");
    var profileColor = document.getElementById("profileColor");
    //change savers that send a request ^_^
    var editUserRequest = document.getElementById("editUserRequest");
    var removeUserRequest = document.getElementById("removeUserRequest");
    var addUserRequest = document.getElementById("addUserRequest");
    //console.
    var addUser = document.getElementById("addUser");
    var editUser = document.getElementById("editUser");
    var removeUser = document.getElementById("removeUser");
    var closeSaveControl = document.getElementById("closeSaveControl");
    var inst;
    //the requests ^_^
    function createSendObj(recipient_id) {
        if (recipient_id === void 0) { recipient_id = ""; }
        var sendObj = {
            name: profileName.value,
            _id: recipient_id,
            account_id: account_id,
            number: profileNumber.value,
            color: profileColor.value
        };
        return (sendObj);
    }
    createColorDropdown();
    //special controls for calender buttons ^_^
    userSelecter === null || userSelecter === void 0 ? void 0 : userSelecter.addEventListener("change", function (event) {
        var target = event.target;
        event.preventDefault();
        profileIndex = parseInt(target.value, 10);
    });
    addUser === null || addUser === void 0 ? void 0 : addUser.addEventListener("click", function (event) {
        event.preventDefault();
        if ((profileName !== null) && (profileNumber !== null) && (profileColor !== null)) {
            profileName.value = "";
            profileNumber.value = "";
            profileColor.selectedIndex = 0;
        }
    });
    editUser === null || editUser === void 0 ? void 0 : editUser.addEventListener("click", function (event) {
        event.preventDefault();
        profileName.value = fakeCalenderProfile[profileIndex].name;
        profileNumber.value = fakeCalenderProfile[profileIndex].number;
        profileColor.value = fakeCalenderProfile[profileIndex].color;
    });
    removeUser === null || removeUser === void 0 ? void 0 : removeUser.addEventListener("click", function (event) {
        event.preventDefault();
    });
    addUserRequest === null || addUserRequest === void 0 ? void 0 : addUserRequest.addEventListener("click", function (event) {
        event.preventDefault();
        var sendObj = {
            name: profileName.value,
            //_id: recipient_id, //we'll get this value from the server :D
            account_id: account_id,
            number: profileNumber.value,
            color: profileColor.value
        };
        var checkResult = addUserCheck(sendObj);
        if (checkResult.pass) {
            addUser_request(sendObj);
        }
        else {
            console.log(checkResult.message);
        }
    });
    editUserRequest === null || editUserRequest === void 0 ? void 0 : editUserRequest.addEventListener("click", function (event) {
        event.preventDefault();
        var recipient_id = fakeCalenderProfile[profileIndex]["_id"];
        var sendObj = createSendObj(recipient_id);
        editUser_request(sendObj);
    });
    removeUserRequest === null || removeUserRequest === void 0 ? void 0 : removeUserRequest.addEventListener("click", function (event) {
        event.preventDefault();
        //const recipient_id=fakeCalenderProfile[profileIndex]["_id"];
        var sendObj = fakeCalenderProfile[profileIndex];
        //alert(fakeCalenderProfile[profileIndex]["name"]);
        //const sendObj=createSendObj(recipient_id);
        removeUser_request(sendObj);
    });
    closeSaveControl === null || closeSaveControl === void 0 ? void 0 : closeSaveControl.addEventListener("click", function (event) {
        event.preventDefault();
        var calenderProfile = document.getElementById("calenderProfile");
        if (calenderProfile !== null) {
            calenderProfile.className = "invisible";
        }
    });
    //profileNumber?.addEventListener("keydown",(event)=>{
    //	phoneNumberFormatter()
    //})
}
function uniqueInput(key, value) {
    for (var f = 0; f < fakeCalenderProfile.length; f++) {
        if (key in fakeCalenderProfile[f]) {
            if (fakeCalenderProfile[f][key] === value) {
                return (false);
            }
        }
    }
    return (true);
}
function addUserCheck(sendObj) {
    console.log('function addUserCheck.  sendObj is: ');
    console.log(sendObj);
    var sendable = true;
    var withoutHyphens = sendObj.number.replace(/-/g, "");
    console.log(withoutHyphens);
    var profileKeys = Object.keys(sendObj);
    var returnObj = {
        message: "User input acceptable",
        pass: true
    };
    if (sendObj.name === "") {
        returnObj.message = "Please input your name";
        returnObj.pass = false;
        return (returnObj);
    }
    if ((withoutHyphens.length > 11)) {
        returnObj.message = "Number is too long";
        returnObj.pass = false;
        return (returnObj);
    }
    if ((withoutHyphens.length < 10)) {
        returnObj.message = "Number is too short";
        returnObj.pass = false;
        return (returnObj);
    }
    for (var p = 0; p < profileKeys.length; p++) {
        if (profileKeys[p] !== "account_id") {
            if (uniqueInput(profileKeys[p], sendObj[profileKeys[p]]) === false) {
                returnObj.message = "This " + profileKeys[p] + " is already being used by someone else";
                returnObj.pass = false;
                return (returnObj);
            }
        }
    }
    return (returnObj);
}
function setupPanelDisplay() {
    var calenderProfile = document.getElementById("calenderProfile"); //a div
    //change savers that send a request ^_^
    var editUserRequest = document.getElementById("editUserRequest");
    var removeUserRequest = document.getElementById("removeUserRequest");
    var addUserRequest = document.getElementById("addUserRequest");
    var controlButtons = document.getElementsByClassName("calenderControl");
    if (controlButtons !== null) { //control visibility for calender
        var _loop_1 = function (n) {
            controlButtons[n].addEventListener("click", function (event) {
                event.preventDefault();
                controlButtons[n];
                if ((controlButtons[n] !== null) && (calenderProfile !== null) && (addUserRequest !== null)
                    && (editUserRequest !== null) && (removeUserRequest !== null) && (controlButtons[n].id != "saveChanges")) {
                    var saveControl = document.getElementsByName("saveControl"); //these buttons will send requests to server ^_^
                    var calenderProfile_1 = document.getElementById("calenderProfile");
                    var profileInput = document.getElementById("profileInput");
                    if (calenderProfile_1 !== null) {
                        if (controlButtons[n].id === "removeUser") {
                            profileInput.className = "invisible";
                        }
                        else {
                            profileInput.className = "";
                        }
                        calenderProfile_1.className = "";
                    }
                    for (var s = 0; s < saveControl.length; s++) {
                        if (controlButtons[n].id === "removeUser") {
                            if (saveControl[s].id === "removeUserRequest") {
                                saveControl[s].className = "";
                            }
                            else {
                                saveControl[s].className = "invisible";
                            }
                        }
                        if (controlButtons[n].id === "editUser") {
                            if (saveControl[s].id === "editUserRequest") {
                                saveControl[s].className = "";
                            }
                            else {
                                saveControl[s].className = "invisible";
                            }
                        }
                        if (controlButtons[n].id === "addUser") {
                            if (saveControl[s].id === "addUserRequest") {
                                saveControl[s].className = "";
                            }
                            else {
                                saveControl[s].className = "invisible";
                            }
                        }
                    }
                }
            });
        };
        for (var n = 0; n < controlButtons.length; n++) {
            _loop_1(n);
        }
    }
}
setupPanelDisplay();
function createCalender(parentId) {
    //alert("diggle")
    var parentElement = document.getElementById(parentId);
    if (parentElement !== null) {
        var hoursInADay = 24;
        var daysInAWeek = 7;
        var weekArray = ['', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var table = document.createElement("table");
        parentElement.appendChild(table);
        var column = 0;
        var row = 0;
        var rowInst = //HTMLTableRowElement;
         void 0; //HTMLTableRowElement;
        var inst = void 0;
        for (var s = 0; s < ((hoursInADay + 1) * (daysInAWeek + 1)); s++) {
            if (row === 0) {
                if (column === 0) {
                    rowInst = document.createElement('TR');
                    rowInst.className = "tableRow";
                    table.appendChild(rowInst);
                }
                inst = document.createElement("TH");
                inst.textContent = weekArray[column];
                rowInst === null || rowInst === void 0 ? void 0 : rowInst.appendChild(inst);
                if ((column > 0) && (rowInst !== undefined)) {
                    inst.className = "weeklyHeader";
                }
            }
            else {
                if (column === 0) {
                    rowInst = document.createElement('TR');
                    rowInst.className = "tableRow";
                    table.appendChild(rowInst);
                }
                inst = document.createElement("TD");
                inst.id = "column" + (column - 1).toString() + "row" + (row - 1).toString();
                rowInst === null || rowInst === void 0 ? void 0 : rowInst.appendChild(inst);
                if (column === 0) {
                    inst.className = "timeCell";
                    inst.textContent = (row - 1).toString() + ":00";
                }
                else {
                    createSlot(inst);
                }
            }
            column++;
            if (column > daysInAWeek) {
                column = 0;
                row++;
            }
        }
    }
}
function markCalender(calenderEntries, profileArray) {
    var name;
    var color;
    var recipient_id;
    for (var c = 0; c < calenderEntries.length; c++) {
        name = calenderEntries[c].name;
        recipient_id = calenderEntries[c].recipient_id;
        color = "";
        for (var p = 0; p < profileArray.length; p++) {
            if ((profileArray[p]._id === recipient_id) && (recipient_id !== "")) {
                color = profileArray[p].color;
                for (var i = 0; i < calenderEntries[c].times.length; i++) {
                    markCalenderSpot(calenderEntries[c].times[i], name, recipient_id, color, calenderEntries);
                }
                break;
            }
        }
    }
}
function createSlot(cell) {
    cell.className = "scheduleCell";
    cell.addEventListener("click", function () {
        if (removedSlot === false) {
            var name_1 = fakeCalenderProfile[profileIndex].name;
            if (fakeCalenderProfile[profileIndex]["_id"] !== undefined) {
                var recipient_id = fakeCalenderProfile[profileIndex]["_id"];
                var color = fakeCalenderProfile[profileIndex].color;
                alert(fakeCalenderEntries.length);
                addEntry(getXY(cell.id), name_1, recipient_id, color, fakeCalenderEntries);
            }
        }
        else {
            removedSlot = false;
        }
    });
}
function getXY(input) {
    var day = Day.SUNDAY;
    var hour = -1;
    var dividingPoint = input.indexOf('row');
    if (dividingPoint !== -1) {
        var first = input.substring(0, dividingPoint);
        var second = input.replace(first, "");
        var nonDigits = /[^\d]*/gim;
        var firstNum = parseInt(first.replace(nonDigits, ""), 10);
        var secondNum = parseInt(second.replace(nonDigits, ""), 10);
        if ((typeof (firstNum) === 'number') && (typeof (secondNum) === 'number')) {
            day = firstNum;
            hour = secondNum;
        }
    }
    return { day: day, hour: hour };
}
function createSlotRequest(pointObj, name, recipient_id) {
    var xhr = new XMLHttpRequest;
    var data = JSON.stringify({
        pointObj: pointObj,
        name: name,
        recipient_id: recipient_id,
        account_id: account_id,
    });
    var url = '/schedule/create_slot';
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            //fakeCalenderEntries=translateToSchedule(xhr.response); //fakeCalenderProfile
            console.log('"/schedule/create_slot" response.  setting fakeCalenderEntries to: ');
            console.log(fakeCalenderEntries);
        }
    };
    xhr.open("POST", url);
    xhr.send(data);
}
function getScheduleRequest() {
    var xhr = new XMLHttpRequest;
    var url = '/schedule/stored_schedule'; //
    var data = JSON.stringify({ account_id: account_id });
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            fakeCalenderEntries = xhr.response;
            //alert("dong!" +fakeCalenderEntries.length);
            var userSelecter = document.getElementById("userSelecter");
            userSelecterSetup(userSelecter);
            createCalender("calenderDiv"); //this must come AFTER profile and Entries have been loaded!
            markCalender(fakeCalenderEntries, fakeCalenderProfile);
            //console.log('fakeCalenderEntries be like: ');
            //console.log(fakeCalenderEntries);
            //console.log('response be like: ');
            //console.log(xhr.response);
            //fakeCalenderEntries=translateToSchedule(xhr.response); //fakeCalenderProfile
            //console.log('"/schedule/stored_schedule" response.  setting fakeCalenderEntries to: ');
            //console.log(fakeCalenderEntries);
        }
    };
    xhr.open("POST", url);
    xhr.send(data);
}
function getProfilesRequest() {
    var xhr = new XMLHttpRequest;
    var url = '/user/stored_profiles';
    var data = JSON.stringify({ account_id: account_id });
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            fakeCalenderProfile = xhr.response;
            //alert("ding!" + fakeCalenderProfile.length);
            setupCalenderButtonControl();
            getScheduleRequest();
            //console.log('fakeCalenderProfile be like: ');
            //console.log(fakeCalenderProfile);
            //console.log('response be like: ');
            //console.log(xhr.response);
            //fakeCalenderProfile=translateToProfile(xhr.response); //fakeCalenderEntries
            //console.log('"/user/stored_profiles" response.  setting fakeCalenderProfile to: ');
            //console.log(fakeCalenderProfile);
        }
    };
    xhr.open("POST", url);
    xhr.send(data);
}
getProfilesRequest();
function removeSlotRequest(pointObj, name, recipient_id) {
    var xhr = new XMLHttpRequest;
    var data = JSON.stringify({
        pointObj: pointObj,
        name: name,
        recipient_id: recipient_id,
        account_id: account_id,
    });
    var url = '/schedule/remove_slot';
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            //fakeCalenderEntries=translateToSchedule(xhr.response); //fakeCalenderProfile
            console.log('"/schedule/remove_slot" response.  setting fakeCalenderEntries to: ');
            console.log(fakeCalenderEntries);
        }
    };
    xhr.open("DELETE", url);
    xhr.send(data);
}
function addUser_request(userInfo) {
    var xhr = new XMLHttpRequest;
    var data = JSON.stringify(userInfo);
    var url = '/user/add';
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            location.reload();
            /*let clearTextArray: string[]=["profileName","profileNumber"];
            let insty: HTMLElement;
            for (let e=0; e < clearTextArray.length; e++){
                insty=document.getElementById(clearTextArray[e]);
                insty.textContent="";
            }

            let clearingArray: string[]=["userSelecter","calenderDiv","profileColor"];
            for(let c=0; c < clearingArray.length; c++){
                removeAllChildNodes(document.getElementById(clearingArray[c]));
            }*/
            //console.log('"/user/add" response.  setting fakeCalenderProfile to: ');
            //fakeCalenderProfile.push(xhr.response); //this gets rid of the need to recall this --> getProfilesRequest()
            //setupCalenderButtonControl();
            //getScheduleRequest();
            //createColorDropdown();
            //console.log(fakeCalenderProfile);
        }
    };
    xhr.open("POST", url);
    xhr.send(data);
}
function editUser_request(userInfo) {
    var xhr = new XMLHttpRequest;
    var data = JSON.stringify(userInfo);
    var url = '/user/edit';
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            //fakeCalenderProfile=translateToProfile(xhr.response); //fakeCalenderEntries
            console.log('editUser_request.  xhr.response is: ');
            console.log(xhr.response);
            createColorDropdown();
            //console.log('"/user/edit" response.  setting fakeCalenderProfile to: ');
            console.log(fakeCalenderProfile);
        }
    };
    xhr.open("POST", url);
    xhr.send(data);
}
function removeUser_request(userInfo) {
    var xhr = new XMLHttpRequest;
    var data = JSON.stringify(userInfo);
    var url = '/user/remove';
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            //fakeCalenderProfile=translateToProfile(xhr.response); //fakeCalenderEntries
            //console.log('"/user/remove" response.  setting fakeCalenderProfile to: ');
            //console.log(fakeCalenderProfile);
            if (xhr.response.success) {
                alert('successful deletion!');
                createColorDropdown();
            }
            else {
                alert('failure to remove.');
            }
        }
    };
    xhr.open("DELETE", url);
    xhr.send(data);
}
/*function translateToProfile(response):CalenderProfileInterface[]{ //used ANY :D

}

function translateToSchedule(response):CalenderEntryInterface[]{ //used ANY :D

} */
//  these two functions were picked up from: 
//  https://tomduffytech.com/how-to-format-phone-number-in-javascript/
//  ?fbclid=IwAR2r_RVijklZQ341e3vTH5YuS-sHKJ42EIhzw0SWJlHS82jP3vFk31ER1Lo
//  and are used to make our phone number look fancy ^_^
/* function formatPhoneNumber(value: string) {
   if (!value) return value;
   const phoneNumber = value.replace(/[^\d]/g, "");
   const phoneNumberLength = phoneNumber.length;
   if (phoneNumberLength < 4) return phoneNumber;
   if (phoneNumberLength < 7) {
     return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
   }
   return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
     3,
     6
   )}-${phoneNumber.slice(6, 9)}`;
 }

 function phoneNumberFormatter() {
   const inputField = document.getElementById("profileNumber") as HTMLInputElement;
   const formattedInputValue = formatPhoneNumber(inputField.value);
   inputField.value = formattedInputValue;
 }*/
var obj = {
    first: "tim",
    last: "e"
};
if ('first' in obj) {
    console.log('bibby!');
}
var me = {
    code: function () { return console.log('Headphones on. Coffee brewed. Editor open.'); },
    name: 'Corrina',
    hobbies: ['Building rockets']
};
me.code();
//# sourceMappingURL=calender.js.map