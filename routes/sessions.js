const express = require('express');
const router = express.Router();
// var Account = require('../models/Account');
var sessions_controller = require('../controllers/sessionsController');
var mongoose = require('mongoose');


// GET route to register
router.get('/register', sessions_controller.register);

//router.post('/register', sessions_controller.process_registration);
router.get('/login', sessions_controller.login);

router.post('/form/register1', sessions_controller.process_registration1);
router.post('/form/register2', sessions_controller.process_registration2);

router.get('/forgot_password', sessions_controller.forgot_password);



module.exports = router;
