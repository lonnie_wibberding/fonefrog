const express = require('express');
const router = express.Router();
var schedule_controller = require('../controllers/scheduleController');



// Routes to change the schedule for forwarding calls for a client

//Get all the schedule information for a specific account
router.get('/index', schedule_controller.stored_schedule);

//Get all the schedule information for a specific account
router.post('/stored_schedule', schedule_controller.stored_schedule)
	// Receives account_id.
	// returns full calendar. Front end code updates as necessary

// Update a specific slot on the calendar with name, day, hour
router.post('/create_slot', schedule_controller.create_slot);
	// Receives CallRecipient id, day, and hour.
	// returns full calendar. Front end code updates as necessary

// Update a specific slot on the calendar with name, day, hour
router.delete('/remove_slot', schedule_controller.remove_slot);
	// Receives Calendar_slot_id
	// returns full calendar. Front end code updates as necessary

module.exports = router;
