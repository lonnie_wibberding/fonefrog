const express = require('express');
const router = express.Router();
var theme_controller = require('../controllers/themeController');

router.get('/about', theme_controller.about);
router.get('/blog-details', theme_controller.blog_details);
router.get('/blog', theme_controller.blog);
router.get('/contact', theme_controller.contact);
router.get('/faq', theme_controller.faq);
router.get('/index-2', theme_controller.index_2);
router.get('/index', theme_controller.index);
router.get('/pricing', theme_controller.pricing);
router.get('/projects-detail', theme_controller.projects_detail);
router.get('/projects', theme_controller.projects);
router.get('/services-detail', theme_controller.services_detail);
router.get('/services', theme_controller.services);
router.get('/shop-details', theme_controller.shop_details);
router.get('/shop', theme_controller.shop);
router.get('/team-details', theme_controller.team_details);
router.get('/team', theme_controller.team);
router.get('/thank-you', theme_controller.thank_you);


module.exports = router;
