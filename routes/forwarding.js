const express = require('express');
const router = express.Router();
var forwarding_controller = require('../controllers/forwardingController');

router.get('/', forwarding_controller.index);

module.exports = router;
