const express = require('express');
const router = express.Router();
var home_controller = require('../controllers/homeController');

router.get('/', home_controller.index);
router.get('/checkout', home_controller.checkout);

module.exports = router;
