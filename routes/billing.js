const express = require('express');
const router = express.Router();
var billing_controller = require('../controllers/billingController');

router.get('/', billing_controller.index);

module.exports = router;
