const express = require('express');
const router = express.Router();
var users_controller = require('../controllers/usersController');
var mongoose = require('mongoose');

router.post('/stored_profiles', users_controller.all);

//Adds a name and number of a person to answer the phone.
router.post('/add', users_controller.add);
	//List of all users returned

//Edits a person who answers the phone.
router.post('/edit', users_controller.edit);
	//List of all users returned

//Removes a person to answer the phone.
router.delete('/remove', users_controller.remove);
	//List of all users returned


module.exports = router;
