const mongoose = require('mongoose');

let accountSchema = new mongoose.Schema({
	time_zone: String,
	trial: {
		type: Boolean,
		default: true
	},
	trial_expiration: Date,
	texting_phone_number: String,
	stripe_customer_id: String,
	customer_first_name: String,
	customer_last_name: String,
	email: String,
	password_hash: String,
	message: String // Here to send a flash message back with the json response if needed.
});

module.exports = mongoose.model('Account', accountSchema);
