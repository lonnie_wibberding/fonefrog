const mongoose = require('mongoose');

let callRecordSchema = new mongoose.Schema({
	account_id: String,
	call_length: Number,
	caller_phone_number: String,
	fone_frog_phone_number: String,
	forwarded_to_number: String,



	time_zone: String,
	trial: {
		type: Boolean,
		default: true
	},
	trial_expiration: Date,
	texting_phone_number: String,
	
	customer_first_name: String,
	customer_last_name: String,
});

module.exports = mongoose.model('CallRecord', callRecordSchema);