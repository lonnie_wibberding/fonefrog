const mongoose = require('mongoose');

let callRecipientSchema = new mongoose.Schema({
	account_id: String,
	name: String,
	number: String,
	color: String
});

module.exports = mongoose.model('CallRecipient', callRecipientSchema);