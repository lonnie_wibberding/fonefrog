const mongoose = require('mongoose');

let calendarSlotsSchema = new mongoose.Schema({
	account_id: String,
	recipient_id: String,
	name: String,
	times: Array
});

module.exports = mongoose.model('CalendarSlots', calendarSlotsSchema);


//STATICS
calendarSlotsSchema.statics.get_calendar = function get_calendar (account_id) {
  //Returns all calendar slots for a specific account.
  CalendarSlots.find({account_id: account_id}).exec()
};
